# Effective Scala
###### Marius Eriksen, Twitter Inc.
###### marius@twitter.com (@marius)

## Table of Contents
* Introduction
* Formatting 
	* Whitespace, Naming, Imports, Braces, Pattern matching, Comments
* Types and Generics 
	* Return type annotations, Variance, Type aliases, Implicits
* Collections
	* Hierarchy, Use, Style, Performance, Java Collections
* Concurrency
	* Futures, Collections
* Control structures
	* Recursion, Returns, for loops and comprehensions, require and assert
* Functional programming
	* Case classes as algebraic data types, Options, Pattern matching, Partial functions, Destructuring bindings, Laziness, Call by name, flatMap
* Object oriented programming
	* Dependency injection, Traits, Visibility, Structural typing
* Error handling
	* Handling exceptions
* Garbage collection
* Java compatibility
* Twitter’s standard libraries
	* Futures, Offer/Broker
* Acknowledgments